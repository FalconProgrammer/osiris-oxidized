# Release Notes

## 0.1.3

- Added DOI badge to readme

## 0.1.2

- Changed Readme/Release Notes to markdown
- Updated readme with usage instructions

## 0.1.1

- Backend Library Changes
  - Number of stability / bug fixes
- Main executable
  - Minimum pupil size is now 1/15th of the image width
    - Now can output:
      - Cropped image of iris with small padding
      - RON file with extracted location of iris/pupil circles/contours.


## 0.1.0

- Initial release
- Segmentation / Normalization of Iris implemented

