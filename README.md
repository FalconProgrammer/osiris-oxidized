# OSIRIS Oxidised

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6499333.svg)](https://doi.org/10.5281/zenodo.6499333)

This is a rust port of OSIRIS v4 with the original source reference from <https://doi.org/10.1016/j.patrec.2015.09.002>. At the time of writing, the original souce code is not available online from the links in the source reference, as such, this is based on code that was included within <https://github.com/CVRL/RaspberryPiOpenSourceIris> repository.

This project also provides a Rust API to use in other projects.

This project is not associated with the original OSIRIS team. It is not compatible with any configuration for the original version.

The current implementation status is as follows:

- [X] Segmentation
- [X] Normalization
- [ ] Recognition

This project was initially developed under the EU Horizon 2020 project [D4FLY](https://d4fly.eu/).

## Requirements

- OpenCV will need to be installed in the system and available. Look at [OpenCV rust instructions](https://github.com/twistedfall/opencv-rust#getting-opencv) on how to install it correctly. 
- A Rust compiler: https://www.rust-lang.org/tools/install

## Compilation

To build the primary executable, run the following command.

``` sh
cargo build --release
```

This will produce the executable in the target/release folder.

## Usage

You can use the `--help` flag to provide a list of arguments. They are also as follows:

- `eye_image_file` - This is a positional argument (must be provided). This is the input image file
- `-m` `--mask` - Outputs a mask of the iris to file
- `-o` `--normalized-circle` - Outputs a normalized image based on the overall circles
- `-c` `--normalized-contour` - Outputs a normalized image based on detected contours
