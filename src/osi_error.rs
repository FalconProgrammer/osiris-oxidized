use opencv as cv;
use thiserror::Error;

#[rustversion::nightly]
#[derive(Error, Debug)]
pub enum OsirisErrorKind {
	#[error("OpenCV Error: {:?}", .source)]
	CvError {
		#[from]
		source: cv::Error,
		backtrace: std::backtrace::Backtrace,
	},

	#[error("Invalid Argument: {0}")]
	InvalidArgument(String),

	#[error("Unknown Error: {0}")]
	OtherError(String),
}

#[rustversion::not(nightly)]
#[derive(Error, Debug)]
pub enum OsirisErrorKind {
	#[error("OpenCV Error: {:?}", .source)]
	CvError {
		#[from]
		source: cv::Error,
	},

	#[error("Invalid Argument: {0}")]
	InvalidArgument(String),

	#[error("Unknown Error: {0}")]
	OtherError(String),
}

pub type OsirisResult<T> = Result<T, OsirisErrorKind>;
