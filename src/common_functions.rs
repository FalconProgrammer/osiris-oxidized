use opencv::core::Point;

/// Convert polar coordinates into cartesian coordinates
pub fn convert_polar_to_cartesion(centre: &Point, radius: i32, theta: f32) -> Point {
	let x = centre.x as f32 + radius as f32 * theta.cos();
	let y = centre.y as f32 - radius as f32 * theta.sin();

	return Point::new(x as i32, y as i32);
}
