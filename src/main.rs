#![cfg_attr(backtrace, feature(backtrace))]

use clap::Parser;
use opencv as cv;
use opencv::imgproc::COLOR_BGR2GRAY;
use opencv::prelude::*;
use osiris_oxidized::*;

#[derive(Parser)]
#[clap(author, version, about)]
struct Args {
	eye_image_file: String,

	#[clap(short, long, help = "Output Mask to File")]
	mask: Option<String>,

	#[clap(short = 'o', long, help = "Output Normalization via Circle to File")]
	normalized_circle: Option<String>,

	#[clap(short = 'c', long, help = "Output Normalization via Contours to File")]
	normalized_contour: Option<String>,

	#[clap(short = 'r', long, help= "Output Cropped iris to file")]
	cropped_iris: Option<String>,

	#[clap(short = 's', long, help= "Output Segmentation Information to File. File format is ron")]
	segmented_data: Option<String>,
}

fn main() -> OsirisResult<()> {
	let args = Args::parse();

	// NOTE (JB) Load up the input image
	let input_image = cv::imgcodecs::imread(&args.eye_image_file, cv::imgcodecs::IMREAD_COLOR)?;

	if input_image.empty() {
		return Err(OsirisErrorKind::InvalidArgument(
			"main: provided image is empty, likely incorrect filename.".to_string()
		))
	}

	// NOTE (JB) Convert to grayscale for OSIRIS
	let gray_input_image = {
		let mut tmp = Mat::default();
		cv::imgproc::cvt_color(&input_image, &mut tmp, cv::imgproc::COLOR_BGR2GRAY, 0)?;

		tmp
	};

	let min_diameter = Some(gray_input_image.cols() / 15);

	// NOTE (JB) Get segmentation information
	let segmented_data = segment_iris(&gray_input_image, None, None, min_diameter, None)?;


	if let Some(s) = &args.segmented_data {
		use std::io::Write;

		let serializable_data = OsirisSerializableData::from(&segmented_data);

		let ron_data = ron::to_string(&serializable_data).expect("Unable to serialize data");

		let mut file = std::fs::File::create(s).expect("Unable to open RON file to output data");
		write!(file, "{}", ron_data).expect("Unable to write to RON file");
	}

	// NOTE (JB) Write mask to file if requested
	if let Some(m) = &args.mask {
		cv::imgcodecs::imwrite(m, &segmented_data.mask, &Default::default())?;
	}

	// NOTE (JB) Normalise by circles and write if requested
	let normalized_circle = normalize_with_segmented(&input_image, &segmented_data, None, None).expect("Error with normalizing iris with circles");

	if let Some(o) = &args.normalized_circle {
		cv::imgcodecs::imwrite(o, &normalized_circle, &Default::default())?;
	}

	// NOTE (JB) Normalise by contours and write if requested
	let normalized_contours = normalize_from_contour_with_segmented(&input_image, &segmented_data, None, None).expect("Error when normalizing iris with contours");

	if let Some(c) = &args.normalized_contour {
		cv::imgcodecs::imwrite(c, &normalized_contours, &Default::default())?;
	}

	// NOTE (JB) Write cropped image to file if requested
	if let Some(c) = &args.cropped_iris {
		let iris = &segmented_data.iris_coarse;

		let padding = (gray_input_image.cols() / 30) / 2;

		let (x, y) = {
			(iris.centre.x - iris.radius - padding,
			 iris.centre.y - iris.radius - padding,)
		};

		let (width, height) = (iris.radius * 2 + padding * 2, iris.radius * 2 + padding * 2);


		let (x, width) = if x < 0 {
			(0, width + x)
		} else {
			(x, width)
		};

		let (y, height) = if y < 0 {
			(0, height + y)
		} else {
			(y, height)
		};

		let width = if x + width > gray_input_image.cols() {
			gray_input_image.cols() - x
		} else {
			width
		};

		let height = if y + width > gray_input_image.rows() {
			gray_input_image.rows() - y
		} else {
			height
		};

		let region = Mat::roi(&gray_input_image, opencv::core::Rect::new(x, y, width, height))?;

		cv::imgcodecs::imwrite(c, &region, &Default::default())?;
	}

	Ok(())
}

