use super::circle::*;
use super::common_functions::*;
use super::float_iterator::FloatIterator;
use super::osi_error::*;

use opencv::core::{
	min_max_loc, no_array, Point, Rect, Scalar, Size, Vec3b, BORDER_CONSTANT, BORDER_DEFAULT, CV_32F, CV_32FC1, CV_8UC1,
};
use opencv::imgproc::{
	fill_convex_poly, morphology_default_border_value, INTER_LINEAR, LINE_8, MORPH_GRADIENT, THRESH_BINARY,
};
use opencv::prelude::*;
use std::cmp::{max, min};

use log::warn;

// Original IMPLEMENTATION : https://github.com/CVRL/RaspberryPiOpenSourceIris/blob/master/OSIRIS_SEGM/src/OsiProcessings.cpp

pub struct OsirisSegmentedData {
	pub pupil_accurate_contour: Vec<Point>,
	pub pupil_accurate_theta: Vec<f32>,
	pub pupil_accurate: OsirisCircle,

	pub pupil_coarse_contour: Vec<Point>,
	pub pupil_coarse_theta: Vec<f32>,
	pub pupil_coarse: OsirisCircle,

	pub iris_accurate_contour: Vec<Point>,
	pub iris_accurate_theta: Vec<f32>,
	pub iris_accurate: OsirisCircle,

	pub iris_coarse_contour: Vec<Point>,
	pub iris_coarse_theta: Vec<f32>,
	pub iris_coarse: OsirisCircle,

	pub mask: Mat,
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct OsirisSerializableData {
	pub iris: SerializableCircle,
	pub iris_contour: Vec<SerializablePoint>,

	pub pupil: SerializableCircle,
	pub pupil_contour: Vec<SerializablePoint>,
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct SerializablePoint {
	pub x: i32,
	pub y: i32,
}

impl From<&Point> for SerializablePoint {
	fn from(p: &Point) -> Self {
		SerializablePoint{ x: p.x, y: p.y }
	}
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct SerializableCircle {
	pub centre: SerializablePoint,
	pub radius: i32
}

impl From<&OsirisCircle> for SerializableCircle {
	fn from(c: &OsirisCircle) -> Self {
		SerializableCircle{centre: (&c.centre).into(), radius: c.radius}
	}
}

impl From<&OsirisSegmentedData> for OsirisSerializableData {
	fn from(data: &OsirisSegmentedData) -> Self {
		OsirisSerializableData {
			iris: (&data.iris_coarse).into(),
			iris_contour: data.iris_coarse_contour.iter().map(|p| p.into()).collect(),
			pupil: (&data.pupil_coarse).into(),
			pupil_contour: data.pupil_coarse_contour.iter().map(|p| p.into()).collect(),
		}
	}
}

const SMALLEST_PUPIL: i32 = 11;
const SMALLEST_IRIS: i32 = 99;
const MAX_RATIO_PUPIL_IRIS: f32 = 0.9;
const MIN_RATIO_PUPIL_IRIS: f32 = 0.2;

pub fn segment_iris(
	eye_image: &Mat,
	p_min_iris_diameter: Option<i32>,
	p_max_iris_diameter: Option<i32>,
	p_min_pupil_diameter: Option<i32>,
	p_max_pupil_diameter: Option<i32>,
) -> OsirisResult<OsirisSegmentedData> {
	//////////////
	// NOTE (JB) Check all parameters for validness, and correct if not
	//////////////

	if eye_image.empty() {
		return Err(OsirisErrorKind::InvalidArgument(
			"segment_iris: Input image is empty.".to_string()
		));
	}
	if eye_image.channels() != 1 {
		return Err(OsirisErrorKind::InvalidArgument(
			"segment_iris: Input image requires image to be 1 channel".to_string(),
		));
	}

	// NOTE (JB) Check Maximum Iris Diameter
	let check_max_iris = std::cmp::min(eye_image.cols(), eye_image.rows());
	let tmp_max_iris_diameter = if let Some(param) = p_max_iris_diameter {
		if param > check_max_iris {
			warn!(
				"Warning in function segment : max_iris_diameter = {} is replaced by {} because image size is {}x{}",
				&param,
				&check_max_iris,
				eye_image.cols(),
				eye_image.rows()
			);
			check_max_iris
		} else {
			param
		}
	} else {
		check_max_iris
	};

	// NOTE (JB) Check Maximum Pupil Diameter
	let check_max_pupil = (MAX_RATIO_PUPIL_IRIS * tmp_max_iris_diameter as f32) as i32;
	let tmp_max_pupil_diameter = if let Some(param) = p_max_pupil_diameter {
		if param > check_max_pupil {
			warn!("Warning in function segment : max_pupil_diameter = {} is replaced by {} because max_iris_diameter = {} and ratio pupil/iris is generally lower than {}", &param, &check_max_pupil, &tmp_max_iris_diameter, MAX_RATIO_PUPIL_IRIS);
			check_max_iris
		} else {
			param
		}
	} else {
		check_max_pupil
	};

	// NOTE (JB) Check Minimum Iris Diameter
	let tmp_min_iris_diameter = if let Some(param) = p_min_iris_diameter {
		if param < SMALLEST_IRIS {
			warn!("Warning in function segment : min_iris_diameter = {} is replaced by {} which is the smallest size for detecting iris", &param, SMALLEST_IRIS);
			SMALLEST_IRIS
		} else {
			param
		}
	} else {
		SMALLEST_IRIS
	};

	// NOTE (JB) Check Minimum Pupil Diameter
	let mut tmp_min_pupil_diameter = if let Some(param) = p_min_pupil_diameter {
		if param < SMALLEST_PUPIL {
			SMALLEST_PUPIL
		} else {
			param
		}
	} else {
		SMALLEST_PUPIL
	};

	// NOTE (JB) FIX: In the original code, this actually changed min_iris_diameter, but comments and warning suggest it should be min_pupil_diameter
	let check_min_pupil = (tmp_min_iris_diameter as f32 * MIN_RATIO_PUPIL_IRIS) as i32;
	tmp_min_pupil_diameter = if tmp_min_pupil_diameter < check_min_pupil {
		warn!("Warning in function segment : min_pupil_diameter = {} is replaced by {} because min_iris_diameter = {} and ratio pupil/iris is generally upper than {}", &tmp_min_pupil_diameter, &check_min_pupil, &tmp_min_iris_diameter, MIN_RATIO_PUPIL_IRIS);
		check_min_pupil
	} else {
		tmp_min_pupil_diameter
	};

	// NOTE (JB) Make all sizes odd
	let min_iris_diameter = tmp_min_iris_diameter + (if (tmp_min_iris_diameter % 2) == 1 { 0 } else { -1 });
	let max_iris_diameter = tmp_max_iris_diameter + (if (tmp_max_iris_diameter % 2) == 1 { 0 } else { 1 });
	let min_pupil_diameter = tmp_min_pupil_diameter + (if (tmp_min_pupil_diameter % 2) == 1 { 0 } else { -1 });
	let max_pupil_diameter = tmp_max_pupil_diameter + (if (tmp_max_pupil_diameter % 2) == 1 { 0 } else { 1 });

	drop(tmp_min_iris_diameter);
	drop(tmp_max_iris_diameter);
	drop(tmp_min_pupil_diameter);
	drop(tmp_min_pupil_diameter);

	//////////////
	// NOTE (JB) Start Processing
	//////////////

	// NOTE (JB) Locate the pupil
	let tmp_pupil = detect_pupil(&eye_image, min_pupil_diameter, max_pupil_diameter)?;

	// NOTE (JB) Fill the holes in an area surrounding pupil

	let clone_image = {
		let tmp_clone_image = eye_image.clone();
		let iris_diameter_34 = 3.0 / 4.0 * (max_iris_diameter as f32);

		let roi_rect = {
			let x = (tmp_pupil.centre.x as f32 - iris_diameter_34 / 2.) as i32;
			let y = (tmp_pupil.centre.y as f32 - iris_diameter_34 / 2.) as i32;
			let width = if x + iris_diameter_34 as i32 >= tmp_clone_image.cols() {
				iris_diameter_34 as i32 - (x + iris_diameter_34 as i32 - tmp_clone_image.cols())
			} else {
				iris_diameter_34 as i32
			};
			let height = if y + iris_diameter_34 as i32 >= tmp_clone_image.rows() {
				iris_diameter_34 as i32 - (y + iris_diameter_34 as i32 - tmp_clone_image.rows())
			} else {
				iris_diameter_34 as i32
			};

			let (x, width) = if x < 0 {
				(0, width + x)
			} else {
				(x, width)
			};

			let (y, height) = if y < 0 {
				(0, height + y)
			} else {
				(y, height)
			};

			Rect::new(x, y, width, height)
		};

		let mut clone_image_roi = Mat::roi(&tmp_clone_image, roi_rect)?;

		fill_white_holes_dst(&Mat::copy(&clone_image_roi)?, &mut clone_image_roi)?;

		tmp_clone_image
	};

	//////////////
	// NOTE (JB) Pupil Accurate Contour
	//////////////

	// NOTE (JB) Will contain samples of angles, in radians
	let theta_step: f32 = 360.0 / std::f32::consts::PI / tmp_pupil.radius as f32;

	let pupil_accurate_theta = FloatIterator::new_with_step(0., 360., theta_step)
		.map(|t| t * std::f32::consts::PI / 180.)
		.collect::<Vec<_>>();

	let pupil_accurate_contour = find_contour(
		&clone_image,
		&tmp_pupil.centre,
		&pupil_accurate_theta,
		tmp_pupil.radius - 20,
		tmp_pupil.radius + 20,
		None,
	)?;

	let pupil_accurate = OsirisCircle::compute_circle_fitting(&pupil_accurate_contour);

	if pupil_accurate.radius * 2 < min_pupil_diameter {
		return Err(OsirisErrorKind::OtherError(
			format!("Accurate contour of pupil indicates diameter [ {} ] is smaller than minimum diameter [ {} ]. Aborting.", pupil_accurate.radius * 2, min_pupil_diameter)
		));
	}

	//////////////
	// NOTE (JB) Pupil Coarse Contour
	//////////////

	let theta_step: f32 = 360.0 / std::f32::consts::PI / tmp_pupil.radius as f32 * 2.;

	let pupil_coarse_theta = FloatIterator::new_with_step(0., 360., theta_step)
		.map(|t| t * std::f32::consts::PI / 180.)
		.collect::<Vec<_>>();

	let pupil_coarse_contour = find_contour(
		&clone_image,
		&tmp_pupil.centre,
		&pupil_coarse_theta,
		tmp_pupil.radius - 20,
		tmp_pupil.radius + 20,
		None,
	)?;

	let pupil_coarse = OsirisCircle::compute_circle_fitting(&pupil_accurate_contour);

	if pupil_coarse.radius * 2 < min_pupil_diameter {
		return Err(OsirisErrorKind::OtherError(
			format!("Coarse contour of pupil indicates diameter [ {} ] is smaller than minimum diameter [ {} ]. Aborting.", pupil_coarse.radius * 2, min_pupil_diameter)
		));
	}

	//////////////
	// NOTE (JB) Mask of Pupil
	//////////////

	let mut mask_pupil =
		Mat::new_rows_cols_with_default(eye_image.rows(), eye_image.cols(), eye_image.typ(), Scalar::from(0.))?;

	draw_contour(&mut mask_pupil, &pupil_accurate_contour, &Scalar::from(255.), -1)?;

	//////////////
	// NOTE (JB) Iris Coarse Contour
	//////////////

	let min_radius = (pupil_coarse.radius as f32 / MAX_RATIO_PUPIL_IRIS).max(min_iris_diameter as f32 / 2.);
	let max_radius = (pupil_coarse.radius as f32 / MIN_RATIO_PUPIL_IRIS).min(3. * max_iris_diameter as f32 / 4.);
	let theta_step: f32 = 360.0 / std::f32::consts::PI / min_radius;

	let iris_coarse_theta = {
		let mut t = 0f32;
		let mut theta = Vec::<f32>::new();

		while t < 360. {
			if t < 180. || (t > 225. && t < 315.) {
				t += 2. * theta_step;
			}

			theta.push(t * std::f32::consts::PI / 180.);

			t += theta_step;
		}

		theta
	};

	let iris_coarse_contour = find_contour(
		&clone_image,
		&pupil_coarse.centre,
		&iris_coarse_theta,
		min_radius as i32,
		max_radius as i32,
		None,
	)?;

	let iris_coarse = OsirisCircle::compute_circle_fitting(&iris_coarse_contour);

	//////////////
	// NOTE (JB) Mask of Iris
	//////////////

	let mut mask_iris =
		Mat::new_rows_cols_with_default(eye_image.rows(), eye_image.cols(), eye_image.typ(), Scalar::from(0.))?;

	draw_contour(&mut mask_iris, &iris_coarse_contour, &Scalar::from(255.), -1)?;

	//////////////
	// NOTE (JB) Iris Accurate Contour
	//////////////

	// For iris accurate contour, limit the search of contour inside a mask
	// mask = dilate(mask-iris) - dilate(mask_pupil)

	// Dilate mask of iris by a disk-shape element
	let mut mask_iris_2 = mask_iris.clone();
	let struct_element = opencv::imgproc::get_structuring_element(
		opencv::imgproc::MORPH_ELLIPSE,
		Size::new(21, 21),
		Point::new(10, 10),
	)?;

	opencv::imgproc::dilate(
		&Mat::copy(&mask_iris_2)?,
		&mut mask_iris_2,
		&struct_element,
		Point::new(-1, -1),
		1,
		BORDER_CONSTANT,
		morphology_default_border_value()?,
	)?;

	drop(struct_element);

	// Dilate the mask of pupil by a horizontal line-shape element
	let mut mask_pupil_2 = mask_pupil.clone();
	let struct_element =
		opencv::imgproc::get_structuring_element(opencv::imgproc::MORPH_ELLIPSE, Size::new(21, 21), Point::new(10, 1))?;
	opencv::imgproc::dilate(
		&Mat::copy(&mask_pupil_2)?,
		&mut mask_pupil_2,
		&struct_element,
		Point::new(-1, -1),
		1,
		BORDER_CONSTANT,
		morphology_default_border_value()?,
	)?;

	drop(struct_element);

	// dilate(mask_iris) - dilate(mask_pupil)
	opencv::core::bitwise_xor(&Mat::copy(&mask_iris_2)?, &mask_pupil_2, &mut mask_iris_2, &no_array())?;

	let theta_step = 360.0 / std::f32::consts::PI / iris_coarse.radius as f32;
	let iris_accurate_theta = FloatIterator::new_with_step(0., 360., theta_step)
		.map(|t| t * std::f32::consts::PI / 180.)
		.collect::<Vec<_>>();

	let iris_accurate_contour = find_contour(
		&clone_image,
		&pupil_coarse.centre,
		&iris_accurate_theta,
		iris_coarse.radius - 50,
		iris_coarse.radius + 20,
		Some(&mask_iris_2),
	)?;

	let iris_accurate = OsirisCircle::compute_circle_fitting(&iris_accurate_contour);

	//////////////
	// NOTE (JB) Mask of Iris based on Accurate Contours
	//////////////

	let mut mask_accurate_iris =
		Mat::new_rows_cols_with_default(eye_image.rows(), eye_image.cols(), eye_image.typ(), Scalar::from(0.))?;

	draw_contour(&mut mask_accurate_iris, &iris_accurate_contour, &Scalar::from(255.), -1)?;

	opencv::core::bitwise_xor(&Mat::copy(&mask_iris)?, &mask_pupil, &mut mask_iris_2, &no_array())?;

	//////////////
	// NOTE (JB) Refine the mask by removing some noise
	//////////////
	let mut safe_area = mask_accurate_iris.clone();
	let safe_pt_1 = Point::new(safe_area.cols() - 1, pupil_coarse.centre.y);
	opencv::imgproc::rectangle_points(
		&mut safe_area,
		Point::new(0, 0),
		safe_pt_1,
		Scalar::from(0.),
		-1,
		LINE_8,
		0,
	)?;

	let safe_pt_2 = Point::new(safe_area.cols() - 1, safe_area.rows() - 1);
	opencv::imgproc::rectangle_points(
		&mut safe_area,
		Point::new(0, pupil_coarse.centre.y + pupil_coarse.radius),
		safe_pt_2,
		Scalar::from(0.),
		-1,
		LINE_8,
		0,
	)?;

	let struct_element =
		opencv::imgproc::get_structuring_element(opencv::imgproc::MORPH_ELLIPSE, Size::new(11, 11), Point::new(5, 5))?;

	opencv::imgproc::erode(
		&Mat::copy(&safe_area)?,
		&mut safe_area,
		&struct_element,
		Point::new(-1, -1),
		1,
		BORDER_CONSTANT,
		morphology_default_border_value()?,
	)?;

	drop(struct_element);

	// Compute the mean and the variance of iris texture inside safe area
	let iris_mean = opencv::core::mean(&eye_image, &safe_area)?.iter().sum::<f64>();
	let mut variance = unsafe { Mat::new_rows_cols(eye_image.rows(), eye_image.cols(), CV_32F)? };
	eye_image.convert_to(&mut variance, CV_32F, 1., 0.)?;

	opencv::core::subtract(
		&Mat::copy(&variance)?,
		&Scalar::from(iris_mean),
		&mut variance,
		&safe_area,
		-1,
	)?;

	opencv::core::multiply(&Mat::copy(&variance)?, &Mat::copy(&variance)?, &mut variance, 1., -1)?;

	let iris_variance = opencv::core::mean(&variance, &safe_area)?.iter().sum::<f64>().sqrt();

	// Build mask of noise : |I-mean| > 2.35 * variance
	let mut mask_noise = eye_image.clone();

	opencv::core::absdiff(&eye_image, &Scalar::from(iris_variance), &mut mask_noise)?;

	opencv::imgproc::threshold(
		&Mat::copy(&mask_noise)?,
		&mut mask_noise,
		2.35 * iris_variance,
		255.,
		THRESH_BINARY,
	)?;

	opencv::core::bitwise_and(&mask_iris, &Mat::copy(&mask_noise)?, &mut mask_noise, &no_array())?;

	// Fusion with accurate contours
	let mut accurate_contours = mask_iris.clone();
	let struct_element =
		opencv::imgproc::get_structuring_element(opencv::imgproc::MORPH_ELLIPSE, Size::new(3, 3), Point::new(1, 1))?;

	opencv::imgproc::morphology_ex(
		&Mat::copy(&accurate_contours)?,
		&mut accurate_contours,
		MORPH_GRADIENT,
		&struct_element,
		Point::new(-1, -1),
		1,
		BORDER_CONSTANT,
		morphology_default_border_value()?,
	)?;

	drop(struct_element);

	let mask_noise = reconstruct_marker_by_mask(&accurate_contours, &mask_noise)?;
	let mut output_mask =
		Mat::new_rows_cols_with_default(eye_image.rows(), eye_image.cols(), CV_8UC1, Scalar::from(0.))?;

	opencv::core::bitwise_xor(&mask_iris, &mask_noise, &mut output_mask, &no_array())?;

	// NOTE (JB) Return!
	Ok(OsirisSegmentedData {
		pupil_accurate_contour,
		pupil_accurate_theta,
		pupil_accurate,
		pupil_coarse_contour,
		pupil_coarse_theta,
		pupil_coarse,
		iris_accurate_contour,
		iris_accurate_theta,
		iris_accurate,
		iris_coarse_contour,
		iris_coarse_theta,
		iris_coarse,
		mask: output_mask,
	})
}

/// Find a contour in image using Viterbi algorithm and anisotropic smoothing
fn find_contour(
	image: &Mat,
	centre: &Point,
	theta: &Vec<f32>,
	min_radius: i32,
	max_radius: i32,
	mask: Option<&Mat>,
) -> OsirisResult<Vec<Point>> {
	// Unwrap the image
	let unwrapped = unwrap_ring(image, centre, min_radius, max_radius, theta)?;

	// Smooth image
	let unwrapped_filtered = process_anistropic_smoothing(&unwrapped, 100, 1.)?;

	// Extract the gradients
	let mut unwrapped_gradients = compute_vertical_gradients(&unwrapped_filtered)?;

	// Take into account the mask
	if let Some(mask) = mask {
		let mask_unwrapped = unwrap_ring(mask, centre, min_radius, max_radius, theta)?;
		let temp = unwrapped_gradients.clone();

		temp.copy_to_masked(&mut unwrapped_gradients, &mask_unwrapped)?;
	}

	let optimal_path = run_viterbi(&unwrapped_gradients)?;

	let contour = (0..optimal_path.len())
		.map(|i| convert_polar_to_cartesion(centre, min_radius + optimal_path[i], theta[i]))
		.collect::<Vec<_>>();

	return Ok(contour);
}

fn process_anistropic_smoothing(image: &Mat, iterations: i32, lambda: f32) -> OsirisResult<Mat> {
	// // Smooth the image by anisotropic smoothing (Gross & Brajovic,2003)
	let mut dst = image.clone();

	//     // Temporary float images
	//     IplImage * tfs = cvCreateImage(cvGetSize(pSrc),IPL_DEPTH_32F,1) ;
	//     cvConvert(pSrc,tfs) ;
	let mut tfs = Mat::default();
	image.convert_to(&mut tfs, CV_32F, 1., 0.)?;

	//     IplImage * tfd = cvCreateImage(cvGetSize(pSrc),IPL_DEPTH_32F,1) ;
	//     cvConvert(pSrc,tfd) ;
	let mut tfd = tfs.clone();

	//     // Make borders dark
	//     cvRectangle(tfd,cvPoint(0,0),cvPoint(tfd->width-1,tfd->height-1),cvScalar(0)) ;
	let rect = Rect::new(0, 0, tfd.cols(), tfd.rows());
	opencv::imgproc::rectangle(&mut tfd, rect, Scalar::from(0.), 1, LINE_8, 0)?;

	let compute_light_image = |tfs: &Mat, tfd: &Mat, i: i32, j: i32| -> OsirisResult<_> {
		// Get pixels in neighbourhood of original image
		// tfsc = ((float*)(tfs->imageData+i*tfs->widthStep))[j] ;
		let tfsc = tfs.at_2d::<f32>(i, j)?;
		// tfsn = ((float*)(tfs->imageData+(i-1)*tfs->widthStep))[j] ;
		let tfsn = tfs.at_2d::<f32>(i - 1, j)?;
		// tfss = ((float*)(tfs->imageData+(i+1)*tfs->widthStep))[j] ;
		let tfss = tfs.at_2d::<f32>(i + 1, j)?;
		// tfse = ((float*)(tfs->imageData+i*tfs->widthStep))[j-1] ;
		let tfse = tfs.at_2d::<f32>(i, j - 1)?;
		// tfsw = ((float*)(tfs->imageData+i*tfs->widthStep))[j+1] ;
		let tfsw = tfs.at_2d::<f32>(i, j + 1)?;

		// Get pixels in neighbourhood of light image
		// tfdn = ((float*)(tfd->imageData+(i-1)*tfd->widthStep))[j] ;
		let tfdn = tfd.at_2d::<f32>(i - 1, j)?;
		// tfds = ((float*)(tfd->imageData+(i+1)*tfd->widthStep))[j] ;
		let tfds = tfd.at_2d::<f32>(i + 1, j)?;
		// tfde = ((float*)(tfd->imageData+i*tfd->widthStep))[j-1] ;
		let tfde = tfd.at_2d::<f32>(i, j - 1)?;
		// tfdw = ((float*)(tfd->imageData+i*tfd->widthStep))[j+1] ;
		let tfdw = tfd.at_2d::<f32>(i, j + 1)?;

		// Compute weber coefficients
		// rhon = min(tfsn,tfsc) / max<float>(1.0,abs(tfsn-tfsc)) ;
		let rhon = tfsn.min(*tfsc) / 1.0f32.max((tfsn - tfsc).abs());
		// rhos = min(tfss,tfsc) / max<float>(1.0,abs(tfss-tfsc)) ;
		let rhos = tfss.min(*tfsc) / 1.0f32.max((tfss - tfsc).abs());
		// rhoe = min(tfse,tfsc) / max<float>(1.0,abs(tfse-tfsc)) ;
		let rhoe = tfse.min(*tfsc) / 1.0f32.max((tfse - tfsc).abs());
		// rhow = min(tfsw,tfsc) / max<float>(1.0,abs(tfsw-tfsc)) ;
		let rhow = tfsw.min(*tfsc) / 1.0f32.max((tfsw - tfsc).abs());

		// Compute LightImage(i,j)
		//                 ((float*)(tfd->imageData+i*tfd->widthStep))[j] = ( ( tfsc + lambda *
		//                     ( rhon * tfdn + rhos * tfds + rhoe * tfde + rhow * tfdw ) )
		//                     / ( 1 + lambda * ( rhon + rhos + rhoe + rhow ) ) ) ;
		return Ok(
			(tfsc + lambda * (rhon * tfdn + rhos * tfds + rhoe * tfde + rhow * tfdw))
				/ (1. + lambda * (rhon + rhos + rhoe + rhow)),
		);
	};

	//     // Loop on iterations
	for _k in 0..iterations {
		// Odd pixels
		for i in 1..tfs.rows() - 1 {
			for j in (2 - i % 2..tfs.cols() - 1).step_by(2) {
				*tfd.at_2d_mut::<f32>(i, j)? = compute_light_image(&tfs, &tfd, i, j)?;
			}
		}

		tfd.copy_to(&mut tfs)?;

		// Even pixels
		for i in 1..tfs.rows() - 1 {
			for j in (1 + i % 2..tfs.cols() - 1).step_by(2) {
				*tfd.at_2d_mut::<f32>(i, j)? = compute_light_image(&tfs, &tfd, i, j)?;
			}
		}

		tfd.copy_to(&mut tfs)?;

		let dst_type = dst.typ();
		tfd.convert_to(&mut dst, dst_type, 1., 0.)?;
	}

	// Borders of image
	for i in 0..tfd.rows() {
		*dst.at_2d_mut::<u8>(i, 0)? = *dst.at_2d::<u8>(i, 1)?;
		*dst.at_2d_mut::<u8>(i, dst.cols() - 1)? = *dst.at_2d::<u8>(i, dst.cols() - 2)?;
	}

	for j in 0..tfd.cols() {
		*dst.at_2d_mut::<u8>(0, j)? = *dst.at_2d::<u8>(1, j)?;
		*dst.at_2d_mut::<u8>(dst.rows() - 1, j)? = *dst.at_2d::<u8>(dst.rows() - 2, j)?;
	}

	Ok(dst)
}

/// Compute vertical gradients using Sobel operator
fn compute_vertical_gradients(image: &Mat) -> OsirisResult<Mat> {
	let mut dst = unsafe { Mat::new_rows_cols(image.rows(), image.cols(), image.typ())? };
	//     // Float values for Sobel
	let mut result_sobel = unsafe { Mat::new_rows_cols(image.rows(), image.cols(), CV_32F)? };

	//     // Sobel filter in vertical direction
	let result_depth = result_sobel.depth();
	opencv::imgproc::sobel(&image, &mut result_sobel, result_depth, 0, 1, 3, 1., 0., BORDER_DEFAULT)?;

	//     // Remove negative edges, ie from white (top) to black (bottom)
	opencv::imgproc::threshold(
		&Mat::copy(&result_sobel).unwrap(),
		&mut result_sobel,
		0.,
		0.,
		opencv::imgproc::THRESH_TOZERO,
	)?;

	//     // Convert into 8-bit
	let mut min: f64 = 0.;
	let mut max: f64 = 0.;
	opencv::core::min_max_loc(&result_sobel, Some(&mut min), Some(&mut max), None, None, &no_array())?;

	let dst_type = dst.typ();
	result_sobel.convert_to(&mut dst, dst_type, 255. / (max - min), -255. * min / (max - min))?;

	return Ok(dst);
}

/// Run viterbi algorithm on gradient (or probability) image and find optimal path
fn run_viterbi(image: &Mat) -> OsirisResult<Vec<i32>> {
	//     // Initialize the output
	let mut optimal_path = vec![0; image.cols() as usize];

	//     // Initialize cost matrix to zero
	let mut cost = Mat::new_rows_cols_with_default(image.rows(), image.cols(), CV_32F, Scalar::from(0.))?;

	//     // Forward process : build the cost matrix
	for w in 0..image.cols() {
		for h in 0..image.rows() {
			//             // First column is same as source image
			if w == 0 {
				*cost.at_2d_mut::<f32>(h, w)? = *image.at_2d::<u8>(h, w)? as f32;
			} else {
				//                 // First line
				if h == 0 {
					//                     ((float*)(cost->imageData+h*cost->widthStep))[w] = max<float>(
					//                     ((float*)(cost->imageData+(h)*cost->widthStep))[w-1],
					//                     ((float*)(cost->imageData+(h+1)*cost->widthStep))[w-1]) +
					//                     ((uchar*)(pSrc->imageData+h*pSrc->widthStep))[w] ;
					*cost.at_2d_mut::<f32>(h, w)? = cost
						.at_2d::<f32>(h, w)?
						.max(*cost.at_2d::<f32>(h + 1, w - 1)? + *image.at_2d::<u8>(h, 2)? as f32)
				}
				//                 // Last line
				else if h == image.rows() - 1 {
					//                     ((float*)(cost->imageData+h*cost->widthStep))[w] = max<float>(
					//                         ((float*)(cost->imageData+h*cost->widthStep))[w-1],
					//                         ((float*)(cost->imageData+(h-1)*cost->widthStep))[w-1]) +
					//                         ((uchar*)(pSrc->imageData+h*pSrc->widthStep))[w] ;
					*cost.at_2d_mut::<f32>(h, w)? = cost
						.at_2d::<f32>(h, w)?
						.max(*cost.at_2d::<f32>(h - 1, w - 1)? + *image.at_2d::<u8>(h, 2)? as f32)
				}
				//                 // Middle lines
				else {
					//                 ((float*)(cost->imageData+h*cost->widthStep))[w] = max<float>(
					//                     ((float*)(cost->imageData+h*cost->widthStep))[w-1],max<float>(
					//                         ((float*)(cost->imageData+(h+1)*cost->widthStep))[w-1],
					//                         ((float*)(cost->imageData+(h-1)*cost->widthStep))[w-1])) +
					//                     ((uchar*)(pSrc->imageData+h*pSrc->widthStep))[w] ;
					*cost.at_2d_mut::<f32>(h, w)? = cost
						.at_2d::<f32>(h, w - 1)?
						.max(cost.at_2d::<f32>(h + 1, w - 1)?.max(*cost.at_2d::<f32>(h - 1, w - 1)?))
						+ *image.at_2d::<u8>(h, w)? as f32;
				}
			}
		}
	}

	let mut max_value = 0.;

	min_max_loc(&cost, None, Some(&mut max_value), None, None, &no_array())?;

	//     // Get the maximum in last column of cost matrix
	let cost_roi = Mat::roi(&cost, Rect::new(cost.cols() - 1, 0, 1, cost.rows()))?;
	let mut max_loc = Point::default();

	opencv::core::min_max_loc(&cost_roi, None, None, None, Some(&mut max_loc), &no_array())?;

	let mut h = max_loc.y;
	let h0 = h;

	drop(cost_roi);

	// Store the point in output vector
	*optimal_path
		.last_mut()
		.ok_or(OsirisErrorKind::OtherError("No element in optimal path?".to_string()))? = h0;

	//
	//     float h1 , h2 , h3 ;
	//
	//     // Backward process
	for w in (0i32..optimal_path.len() as i32 - 1).rev() {
		// Constraint to close the contour
		if h - h0 > w {
			h -= 1;
		} else if h0 - h > w {
			h += 1;
		} else {
			// When no need to constraint : use the cost matrix

			// h1 is the value above line h
			let h1 = if h == 0 { 0f32 } else { *cost.at_2d::<f32>(h - 1, w)? };

			// h2 is the value at line h
			let h2 = *cost.at_2d::<f32>(h, w)?;

			// h3 is the value below line h
			let h3 = if h == cost.rows() - 1 {
				0f32
			} else {
				*cost.at_2d::<f32>(h + 1, w)?
			};

			// h1 is the maximum => h decreases
			if h1 > h2 && h1 > h3 {
				h -= 1;
			} else if h3 > h2 && h3 > h1 {
				h += 1;
			}
		}

		optimal_path[w as usize] = h;
	}

	return Ok(optimal_path);
}

/// Unwrap a ring into a rectangular band
pub fn unwrap_ring(
	image: &Mat,
	centre: &Point,
	min_radius: i32,
	max_radius: i32,
	theta: &Vec<f32>,
) -> OsirisResult<Mat> {
	// TODO (JB) Make work for all channels

	if image.channels() != 1 && image.channels() != 3 {
		return Err(OsirisErrorKind::InvalidArgument(
			"unwrap_ring: Input image requires image to be 1/3 channels".to_string(),
		));
	}

	// Result image
	let mut result = Mat::new_size_with_default(
		opencv::core::Size::new(theta.len() as i32, max_radius - min_radius + 1),
		image.typ(),
		Scalar::from(0.),
	)?;

	// Loop on columns of normalized image
	for j in 0..result.cols() {
		// Loop on lines of normalized image
		for i in 0..result.rows() {
			let point = convert_polar_to_cartesion(
				&centre,
				min_radius + i,
				*theta
					.get(j as usize)
					.ok_or(OsirisErrorKind::OtherError("Invalid Theta".to_string()))?,
			);

			// Do not exceed image size
			if point.x >= 0 && point.x < image.cols() && point.y >= 0 && point.y < image.rows() {
				if image.channels() == 1 {
					*result.at_2d_mut::<u8>(i, j)? = *image.at_2d::<u8>(point.y, point.x)?;
				} else if image.channels() == 3 {
					*result.at_2d_mut::<Vec3b>(i, j)? = *image.at_2d::<Vec3b>(point.y, point.x)?;
				}
			}
		}
	}

	return Ok(result);
}

/// Draw a contour (vector of CvPoint) on an image
fn draw_contour(image: &mut Mat, contour: &Vec<Point>, colour: &Scalar, thickness: i32) -> OsirisResult<()> {
	// Draw INSIDE the contour if thickness is negative
	if thickness < 0 {
		let cv_points = opencv::core::Vector::<Point>::from_iter(contour.iter().cloned());
		fill_convex_poly(image, &cv_points, *colour, LINE_8, 0)?;
	} else {
		// Draw the contour on binary mask
		let mut mask = Mat::new_rows_cols_with_default(image.rows(), image.cols(), CV_8UC1, Scalar::from(0.))?;

		for c in contour {
			// Do not exceed image sizes
			let x = min(max(0, c.x), image.cols());
			let y = min(max(0, c.y), image.rows());

			// Plot the point on image
			*mask.at_2d_mut::<u8>(y, x).unwrap() = 255;
		}

		// Dilate mask if user specified thickness
		if thickness > 1 {
			let se = opencv::imgproc::get_structuring_element(
				opencv::imgproc::MORPH_ELLIPSE,
				Size::new(3, 3),
				Point::new(1, 1),
			)?;

			opencv::imgproc::dilate(
				&Mat::copy(&mask)?,
				&mut mask,
				&se,
				Point::new(-1, -1),
				1,
				BORDER_CONSTANT,
				morphology_default_border_value()?,
			)?;
		}

		// Color rgb
		image.set_to(&colour, &mask)?;
	}

	Ok(())
}

fn detect_pupil(image: &Mat, p_min_pupil_diameter: i32, p_max_pupil_diameter: i32) -> OsirisResult<OsirisCircle> {
	let check_max_pupil = (min(image.rows(), image.cols()) as f32 * MAX_RATIO_PUPIL_IRIS) as i32;

	let max_pupil_diameter = if p_max_pupil_diameter > check_max_pupil {
		warn!("Warning in function detect_pupil : max_pupil_diameter = {} is replaced by {} because image size is {}x{} and ratio pupil/iris is generally lower than {}",
				 &p_max_pupil_diameter, &check_max_pupil, image.cols(), image.rows(), MIN_RATIO_PUPIL_IRIS);
		check_max_pupil
	} else {
		p_max_pupil_diameter
	};

	// NOTE (JB) Resize Image
	let scale: f32 = SMALLEST_PUPIL as f32 / p_min_pupil_diameter as f32;
	let mut resized_image = unsafe {
		Mat::new_rows_cols(
			(image.rows() as f32 * scale) as i32,
			(image.cols() as f32 * scale) as i32,
			image.typ(),
		)?
	};

	let resized_size = resized_image.size()?;
	opencv::imgproc::resize(image, &mut resized_image, resized_size, 0., 0., INTER_LINEAR)?;

	// NOTE (JB) Rescale Sizes
	let min_pupil_diameter = (p_min_pupil_diameter as f32 * scale) as i32;
	let max_pupil_diameter = (max_pupil_diameter as f32 * scale) as i32;

	let min_pupil_diameter = min_pupil_diameter + (if (min_pupil_diameter % 2) == 1 { 0 } else { 1 });
	let max_pupil_diameter = max_pupil_diameter + (if (max_pupil_diameter % 2) == 1 { 0 } else { -1 });

	// NOTE (JB) Fill Holes
	let filled = fill_white_holes(&resized_image)?;

	// NOTE (JB) Gradients
	let mut gradient_horizontal = unsafe { Mat::new_rows_cols(filled.rows(), filled.cols(), opencv::core::CV_32F)? };
	let mut gradient_vertical = unsafe { Mat::new_rows_cols(filled.rows(), filled.cols(), opencv::core::CV_32F)? };

	let mut filled_32 = unsafe { Mat::new_rows_cols(filled.rows(), filled.cols(), opencv::core::CV_32F)? };

	filled.convert_to(&mut filled_32, CV_32FC1, 1., 0.)?;

	opencv::imgproc::sobel(
		&filled_32,
		&mut gradient_horizontal,
		CV_32F,
		1,
		0,
		3,
		1.,
		0.,
		BORDER_DEFAULT,
	)?;

	opencv::imgproc::sobel(
		&filled_32,
		&mut gradient_vertical,
		CV_32F,
		0,
		1,
		3,
		1.,
		0.,
		BORDER_DEFAULT,
	)?;

	// NOTE (JB) Normalize Gradients
	let mut gh2 = unsafe { Mat::new_size(filled.size()?, opencv::core::CV_32F)? };
	opencv::core::multiply(&gradient_horizontal, &gradient_horizontal, &mut gh2, 1., -1)?;

	let mut gv2 = unsafe { Mat::new_size(filled.size()?, opencv::core::CV_32F)? };
	opencv::core::multiply(&gradient_vertical, &gradient_vertical, &mut gv2, 1., -1)?;

	let mut gradient_normalized = unsafe { Mat::new_size(filled.size()?, opencv::core::CV_32F)? };
	opencv::core::add(&gh2, &gv2, &mut gradient_normalized, &no_array(), -1)?;
	opencv::core::pow(&Mat::copy(&gradient_normalized)?, 0.5, &mut gradient_normalized)?;

	opencv::core::divide2(
		&Mat::copy(&gradient_horizontal).unwrap(),
		&gradient_normalized,
		&mut gradient_horizontal,
		1.,
		-1,
	)?;

	opencv::core::divide2(
		&Mat::copy(&gradient_vertical).unwrap(),
		&gradient_normalized,
		&mut gradient_vertical,
		1.,
		-1,
	)?;

	// NOTE (JB) Create the filters fh and fv
	let filter_size: i32 = max_pupil_diameter + if max_pupil_diameter % 2 == 1 { 0 } else { -1 };
	let mut fh = unsafe { Mat::new_rows_cols(filter_size, filter_size, opencv::core::CV_32F)? };
	let mut fv = unsafe { Mat::new_rows_cols(filter_size, filter_size, opencv::core::CV_32F)? };

	for i in 0..filter_size {
		let x: f32 = i as f32 - (filter_size as f32 - 1.) / 2.;
		for j in 0..filter_size {
			let y: f32 = j as f32 - (filter_size as f32 - 1.) / 2.;

			if x != 0. || y != 0. {
				*fh.at_2d_mut::<f32>(i, j)? = y / (x * x + y * y).sqrt();
				*fv.at_2d_mut::<f32>(i, j)? = x / (x * x + y * y).sqrt();
			} else {
				*fh.at_2d_mut::<f32>(i, j)? = 0.;
				*fv.at_2d_mut::<f32>(i, j)? = 0.;
			}
		}
	}

	// NOTE (JB) Create the mask
	let mut mask = unsafe { Mat::new_rows_cols(filter_size, filter_size, CV_8UC1)? };

	// NOTE (JB) Temporary matrix for masking the filter (later: tempfilter = filter * mask)
	let mut temp_filter = unsafe { Mat::new_rows_cols(filter_size, filter_size, CV_32FC1)? };

	let mut old_max_val: f64 = 0.;
	let mut rpupil = OsirisCircle {
		centre: Point::default(),
		radius: 0,
	};

	// NOTE (JB) Somewhere in the equations above, NaNs are introduced. In order for the rest to not
	//           produce tonnes of +INF and -INF values, we need to replace them.
	opencv::core::patch_na_ns(&mut gradient_horizontal, 0.)?;
	opencv::core::patch_na_ns(&mut gh2, 0.)?;
	opencv::core::patch_na_ns(&mut gradient_vertical, 0.)?;
	opencv::core::patch_na_ns(&mut gv2, 0.)?;
	opencv::core::patch_na_ns(&mut gradient_normalized, 0.)?;

	let gh2_depth = gh2.depth();

	// NOTE (JB) Multi Resolution of radius
	for r in (SMALLEST_PUPIL - 1) / 2..(max_pupil_diameter - 1) / 2 {

		// TODO (JB) BUG FIX to ensure arithmetic of smallest pupil is correct. Should fix earlier
		//                   on in code.
		let final_diameter = (r as f32 / scale) * 2.;
		if final_diameter < p_min_pupil_diameter as f32 {
			continue
		}

		// NOTE (JB) Centred ring with radius r and width = 2
		mask.set(Scalar::from(0.)).unwrap();

		opencv::imgproc::circle(
			&mut mask,
			Point::new((filter_size - 1) / 2, (filter_size - 1) / 2),
			r,
			Scalar::from(1.),
			2,
			LINE_8,
			0,
		)?;

		// NOTE (JB) Fh * Gh
		temp_filter.set(Scalar::from(0.))?;
		fh.copy_to_masked(&mut temp_filter, &mask)?;

		opencv::imgproc::filter_2d(
			&gradient_horizontal,
			&mut gh2,
			-1,
			&temp_filter,
			Point::new(-1, -1),
			0.,
			BORDER_DEFAULT,
		)?;

		// NOTE (JB) Fv * Gv
		temp_filter.set(Scalar::from(0.))?;
		fv.copy_to_masked(&mut temp_filter, &mask)?;

		opencv::imgproc::filter_2d(
			&gradient_vertical,
			&mut gv2,
			-1,
			&temp_filter,
			Point::new(-1, -1),
			0.,
			BORDER_DEFAULT,
		)?;

		// NOTE (JB) Fh * Gh + Fv * Gv
		opencv::core::add(&gh2, &gv2, &mut gradient_normalized, &no_array(), -1)?;

		Mat::copy(&gradient_normalized)?.convert_to(
			&mut gradient_normalized,
			gh2_depth,
			1.0 / opencv::core::sum_elems(&mask)?.0[0],
			0.,
		)?;

		// NOTE (JB) Sum in the disk-shaped neighbourhood
		mask.set(Scalar::from(0.))?;

		opencv::imgproc::circle(
			&mut mask,
			Point::new((filter_size - 1) / 2, (filter_size - 1) / 2),
			r,
			Scalar::from(1.),
			-1,
			LINE_8,
			0,
		)?;

		opencv::imgproc::filter_2d(
			&filled,
			&mut gh2,
			gh2_depth,
			&mask,
			Point::new(-1, -1),
			0.,
			BORDER_DEFAULT,
		)?;

		Mat::copy(&gh2)?.convert_to(
			&mut gh2,
			gh2_depth,
			-1.0 / opencv::core::sum_elems(&mask).unwrap().0[0] / 255.0,
			0.,
		)?;

		// NOTE (JB) Add the two features: contour + darkness
		opencv::core::add(
			&Mat::copy(&gradient_normalized)?,
			&gh2,
			&mut gradient_normalized,
			&no_array(),
			-1,
		)?;

		// NOTE (JB) Find the maximum in feature image
		let mut max_val: f64 = 0.;
		let mut max_loc = Point::default();

		opencv::core::min_max_loc(
			&gradient_normalized,
			None,
			Some(&mut max_val),
			None,
			Some(&mut max_loc),
			&no_array(),
		)?;

		if max_val > old_max_val {
			old_max_val = max_val;
			rpupil.radius = r;
			rpupil.centre = max_loc;
		}
	}

	if rpupil.radius == 0 {
		return Err(OsirisErrorKind::OtherError(
			"Unable to find pupil in image".to_string()
		));
	}

	// NOTE (JB) Rescale Circle
	let x: i32 =
		((rpupil.centre.x * (image.cols() - 1)) as f32 / (filled.cols() - 1) as f32 + ((1.0 / scale) - 1.) / 2.) as i32;
	let y: i32 =
		((rpupil.centre.y * (image.rows() - 1)) as f32 / (filled.rows() - 1) as f32 + ((1.0 / scale) - 1.) / 2.) as i32;

	rpupil.radius = (rpupil.radius as f32 / scale) as i32;
	rpupil.centre = Point::new(x, y);

	Ok(rpupil)
}

fn fill_white_holes(image: &Mat) -> OsirisResult<Mat> {
	let (width, height) = (image.cols(), image.rows());

	let mut dst = unsafe { Mat::new_rows_cols(width, height, image.typ())? };

	fill_white_holes_dst(image, &mut dst)?;

	Ok(dst)
}

fn fill_white_holes_dst(image: &Mat, dst: &mut Mat) -> OsirisResult<()> {
	let (width, height) = (image.cols(), image.rows());

	// NOTE (JB) Mask for Reconstruction: image + borders=0
	let mask = Mat::new_rows_cols_with_default(height + 2, width + 2, image.typ(), opencv::core::Scalar::from(0.))?;

	let mut mask_dst_roi = Mat::roi(&mask, opencv::core::Rect::new(1, 1, width, height))?;
	image.copy_to(&mut mask_dst_roi)?;

	drop(mask_dst_roi);

	// NOTE (JB) Marker for reconstruction: all=0 + borders=255
	let mut marker =
		Mat::new_rows_cols_with_default(height + 2, width + 2, image.typ(), opencv::core::Scalar::from(0.))?;

	opencv::imgproc::rectangle(
		&mut marker,
		opencv::core::Rect::new(1, 1, width + 1, width + 1),
		opencv::core::Scalar::from(255.),
		1,
		LINE_8,
		0,
	)?;

	let result = reconstruct_marker_by_mask(&marker, &mask)?;

	// NOTE (JB) Remove borders
	let result_roi = Mat::roi(&result, opencv::core::Rect::new(1, 1, width, height))?;
	result_roi.copy_to(dst)?;

	Ok(())
}

fn reconstruct_marker_by_mask(pmarker: &Mat, pmask: &Mat) -> OsirisResult<Mat> {
	let mut difference = pmask.clone();
	let mask = pmask.clone();
	let mut dst = pmarker.clone();

	// NOTE (JB) Structuring element for morphological operation
	let element =
		opencv::imgproc::get_structuring_element(opencv::imgproc::MORPH_ELLIPSE, Size::new(3, 3), Point::new(1, 1))?;

	// NOTE (JB) Will stop when the marker does not change anymore
	while opencv::core::sum_elems(&difference)?.0[0] != 0. {
		dst.copy_to(&mut difference)?;

		// NOTE (JB) Anchor onwards are default values
		opencv::imgproc::dilate(
			&Mat::copy(&dst).unwrap(),
			&mut dst,
			&element,
			Point::new(-1, -1),
			1,
			BORDER_CONSTANT,
			morphology_default_border_value().unwrap(),
		)?;

		opencv::core::min(&Mat::copy(&dst)?, &mask, &mut dst)?;

		opencv::core::absdiff(&dst, &Mat::copy(&difference)?, &mut difference)?;
	}

	Ok(dst)
}

// #[cfg(test)]
// mod tests {
// 	use crate::opencv_segment::draw_contour;
// 	use crate::opencv_segment::{segment_iris, unwrap_ring};
// 	use opencv::core::{no_array, Scalar};
// 	use opencv::imgcodecs::{imread, imwrite, IMREAD_COLOR};
// 	use opencv::imgproc::{cvt_color, COLOR_BGR2GRAY};
// 	use opencv::prelude::*;
//
// 	#[test]
// 	fn load_image_test() {
// 		let _image = imread(
// 			&format!("{}/test_res/000001.tiff", env!("CARGO_MANIFEST_DIR")),
// 			IMREAD_COLOR,
// 		)
// 		.unwrap();
// 	}
//
// 	#[test]
// 	fn segment_run_test() {
// 		let mut image = imread(
// 			&format!("{}/test_res/000001.tiff", env!("CARGO_MANIFEST_DIR")),
// 			IMREAD_COLOR,
// 		)
// 		.unwrap();
// 		let mut gray_image = image.clone();
// 		cvt_color(&Mat::copy(&image).unwrap(), &mut gray_image, COLOR_BGR2GRAY, 0);
//
// 		assert_eq!(gray_image.channels(), 1);
//
// 		let output = segment_iris(&gray_image, None, None, None, None).unwrap();
//
// 		let mut min: f64 = 0.;
// 		let mut max: f64 = 0.;
// 		opencv::core::min_max_loc(&output.mask, Some(&mut min), Some(&mut max), None, None, &no_array());
//
// 		imwrite("test_mask.png", &output.mask, &opencv::core::Vector::new());
//
// 		draw_contour(&mut gray_image, &output.iris_coarse_contour, &Scalar::from(255.), 1);
// 		imwrite("contoured.png", &gray_image, &opencv::core::Vector::new());
//
// 		let unwrapped = unwrap_ring(
// 			&image,
// 			&output.iris_coarse.centre,
// 			output.pupil_coarse.radius,
// 			output.iris_coarse.radius,
// 			&output.iris_coarse_theta,
// 		)
// 		.unwrap();
// 		imwrite("unwrapped.png", &unwrapped, &Default::default());
// 	}
// }
