/// produces: [ linear_interpol(start, end, i/steps) | i <- 0..steps ]
/// (does NOT include "end")
///
/// linear_interpol(a, b, p) = (1 - p) * a + p * b
pub struct FloatIterator {
	current: u64,
	current_back: u64,
	steps: u64,
	start: f32,
	end: f32,
	step: Option<f32>,
}

impl FloatIterator {
	pub fn new(start: f32, end: f32, steps: u64) -> Self {
		FloatIterator {
			current: 0,
			current_back: steps,
			steps: steps,
			start: start,
			end: end,
			step: None,
		}
	}

	/// calculates number of steps from (end - start) / step
	pub fn new_with_step(start: f32, end: f32, step: f32) -> Self {
		let steps = ((end - start) / step).abs().ceil() as u64;
		FloatIterator {
			current: 0,
			current_back: steps,
			steps: steps,
			start: start,
			end: end,
			step: Some(step),
		}
	}

	pub fn length(&self) -> u64 {
		self.current_back - self.current
	}

	fn at(&self, pos: u64) -> f32 {
		if let Some(step) = self.step {
			self.start + step * pos as f32
		} else {
			let f_pos = pos as f32 / self.steps as f32;
			(1. - f_pos) * self.start + f_pos * self.end
		}
	}

	/// panics (in debug) when len doesn't fit in usize
	fn usize_len(&self) -> usize {
		let l = self.length();
		debug_assert!(l <= usize::MAX as u64);
		l as usize
	}
}

impl Iterator for FloatIterator {
	type Item = f32;

	fn next(&mut self) -> Option<Self::Item> {
		if self.current >= self.current_back {
			return None;
		}
		let result = self.at(self.current);
		self.current += 1;
		Some(result)
	}

	fn size_hint(&self) -> (usize, Option<usize>) {
		let l = self.usize_len();
		(l, Some(l))
	}

	fn count(self) -> usize {
		self.usize_len()
	}
}

impl DoubleEndedIterator for FloatIterator {
	fn next_back(&mut self) -> Option<Self::Item> {
		if self.current >= self.current_back {
			return None;
		}
		self.current_back -= 1;
		let result = self.at(self.current_back);
		Some(result)
	}
}

impl ExactSizeIterator for FloatIterator {
	fn len(&self) -> usize {
		self.usize_len()
	}

	//fn is_empty(&self) -> bool {
	//    self.length() == 0u64
	//}
}
