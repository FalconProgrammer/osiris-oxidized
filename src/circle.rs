use opencv::core::Point;

#[derive(Debug)]
pub struct OsirisCircle {
	pub centre: opencv::core::Point,
	pub radius: i32,
}

impl OsirisCircle {
	#[allow(unused_assignments)]
	pub fn compute_circle_fitting(points: &Vec<Point>) -> OsirisCircle {
		// Compute the averages mx and my
		let mut mx = 0f32;
		let mut my = 0f32;

		for p in points {
			mx += p.x as f32;
			my += p.y as f32;
		}

		let mx = mx / points.len() as f32;
		let my = my / points.len() as f32;

		// NOTE (JB) Macro just allows for easy f32 initialization. Yes, I'm lazy.
		macro_rules! fc {
			($name: ident) => {
				let mut $name = 0f32;
			};
		}

		// Work in (u,v) space, with u = x-mx and v = y-my
		fc!(u);
		fc!(v);
		fc!(suu);
		fc!(svv);
		fc!(suv);
		fc!(suuu);
		fc!(svvv);
		fc!(suuv);
		fc!(suvv);

		// Build some sums
		for p in points {
			u = p.x as f32 - mx;
			v = p.y as f32 - my;

			suu += u * u;
			svv += v * v;
			suv += u * v;
			suuu += u * u * u;
			svvv += v * v * v;
			suuv += u * u * v;
			suvv += u * v * v;
		}

		// These equations are demonstrated in paper from R.Bullock (2006)
		let uc = 0.5 * (suv * (svvv + suuv) - svv * (suuu + suvv)) / (suv * suv - suu * svv);
		let vc = 0.5 * (suv * (suuu + suvv) - suu * (svvv + suuv)) / (suv * suv - suu * svv);

		let centre = Point::new((uc + mx) as i32, (vc + my) as i32);
		let radius = (uc * uc + vc * vc + (suu + svv) / points.len() as f32).sqrt() as i32;

		OsirisCircle { centre, radius }
	}
}
