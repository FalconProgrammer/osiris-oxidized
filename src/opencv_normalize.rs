use opencv::core::{Point, Vec3b};
use opencv::prelude::*;

use super::common_functions::*;
use super::opencv_segment::OsirisSegmentedData;
use super::osi_error::*;
use crate::OsirisCircle;

pub fn normalize_with_segmented(
	image: &Mat,
	segmented_data: &OsirisSegmentedData,
	width: Option<i32>,
	height: Option<i32>,
) -> OsirisResult<Mat> {
	let width = if let Some(width) = width {
		width
	} else {
		(2.0 * segmented_data.iris_coarse.radius as f32 * std::f32::consts::PI) as i32
	};

	let height = if let Some(height) = height {
		height
	} else {
		(segmented_data.iris_coarse.radius - segmented_data.pupil_coarse.radius) as i32
	};

	normalize(
		image,
		&segmented_data.pupil_coarse,
		&segmented_data.iris_coarse,
		width,
		height,
	)
}

pub fn normalize(image: &Mat, pupil: &OsirisCircle, iris: &OsirisCircle, width: i32, height: i32) -> OsirisResult<Mat> {
	if image.channels() != 1 && image.channels() != 3 {
		return Err(OsirisErrorKind::InvalidArgument(
			"normalize: Input image requires image to be 1/3 channels".to_string(),
		));
	}

	// NOTE (JB) Create Destination Matrix
	let mut dst = Mat::zeros(height, width, image.typ())?.to_mat()?;

	// Loop on columns of normalized src
	for j in 0..width {
		// One column correspond to an angle theta
		let theta = (j as f32 / width as f32) * (2. * std::f32::consts::PI);

		// Coordinates relative to both centers: iris and pupil
		let point_pupil = convert_polar_to_cartesion(&pupil.centre, pupil.radius, theta);
		let point_iris = convert_polar_to_cartesion(&iris.centre, iris.radius, theta);

		// Loop on lines of normalized src
		for i in 0..height {
			// The radial parameter
			let radius = i as f32 / height as f32;

			// Coordinates relative to both radii : iris and pupil
			let x = ((1. - radius) * point_pupil.x as f32 + radius * point_iris.x as f32) as i32;
			let y = ((1. - radius) * point_pupil.y as f32 + radius * point_iris.y as f32) as i32;

			if x >= 0 && x < image.cols() && y >= 0 && y < image.rows() {
				if dst.channels() == 1 {
					*dst.at_2d_mut::<u8>(i, j)? = *image.at_2d::<u8>(y, x)?;
				} else if dst.channels() == 3 {
					*dst.at_2d_mut::<Vec3b>(i, j)? = *image.at_2d::<Vec3b>(y, x)?;
				}
			}
		}
	}

	Ok(dst)
}

pub fn normalize_from_contour_with_segmented(
	image: &Mat,
	segmented_data: &OsirisSegmentedData,
	width: Option<i32>,
	height: Option<i32>,
) -> OsirisResult<Mat> {
	let width = if let Some(width) = width {
		width
	} else {
		(2.0 * segmented_data.iris_coarse.radius as f32 * std::f32::consts::PI) as i32
	};

	let height = if let Some(height) = height {
		height
	} else {
		(segmented_data.iris_coarse.radius - segmented_data.pupil_coarse.radius) as i32
	};

	normalize_from_contour(
		image,
		&segmented_data.pupil_coarse_contour,
		&segmented_data.iris_coarse_contour,
		&segmented_data.pupil_coarse_theta,
		&segmented_data.iris_coarse_theta,
		width,
		height,
	)
}

pub fn normalize_from_contour(
	image: &Mat,
	pupil_coarse_contour: &Vec<Point>,
	iris_coarse_contour: &Vec<Point>,
	theta_coarse_pupil: &Vec<f32>,
	theta_coarse_iris: &Vec<f32>,
	width: i32,
	height: i32,
) -> OsirisResult<Mat> {
	if image.channels() != 1 && image.channels() != 3 {
		return Err(OsirisErrorKind::InvalidArgument(
			"normalize: Input image requires image to be 1/3 channels".to_string(),
		));
	}

	// NOTE (JB) Create Destination Matrix
	let mut dst = Mat::zeros(height, width, image.typ())?.to_mat()?;

	for j in 0..width {
		let theta = (j as f32 / width as f32) * (2. * std::f32::consts::PI);

		let point_pupil = interpolate(&pupil_coarse_contour, &theta_coarse_pupil, theta);
		let point_iris = interpolate(&iris_coarse_contour, &theta_coarse_iris, theta);

		let point_iris = Point::new(num::clamp(point_iris.x, 0, image.cols()), num::clamp(point_iris.y, 0, image.rows()));

		for i in 0..height {
			// The radial parameter
			let radius = i as f32 / height as f32;

			// Coordinates relative to both radii : iris and pupil
			let x = ((1. - radius) * point_pupil.x as f32 + radius * point_iris.x as f32) as i32;
			let y = ((1. - radius) * point_pupil.y as f32 + radius * point_iris.y as f32) as i32;

			if x >= 0 && x < image.cols() && y >= 0 && y < image.rows() {
				if dst.channels() == 1 {
					*dst.at_2d_mut::<u8>(i, j)? = *image.at_2d::<u8>(y, x)?;
				} else if dst.channels() == 3 {
					*dst.at_2d_mut::<Vec3b>(i, j)? = *image.at_2d::<Vec3b>(y, x)?;
				}
			}
		}
	}

	Ok(dst)
}

fn interpolate(coarse_contour: &Vec<Point>, coarse_theta: &Vec<f32>, theta: f32) -> Point {
	let (interpolation, i1, i2) = if theta < coarse_theta[0] {
		let tmp_i1 = (coarse_theta.len() - 1);
		let tmp_i2 = 0;
		let tmp_interp = (theta - (coarse_theta[tmp_i1] - 2. * std::f32::consts::PI))
			/ (coarse_theta[tmp_i2] - (coarse_theta[tmp_i1] - 2. * std::f32::consts::PI));

		(tmp_interp, tmp_i1, tmp_i2)
	} else if theta >= *coarse_theta.last().unwrap() {
		let tmp_i1 = (coarse_theta.len() - 1);
		let tmp_i2 = 0;
		let tmp_interp =
			(theta - coarse_theta[tmp_i1]) / (coarse_theta[tmp_i2] + 2. * std::f32::consts::PI - coarse_theta[tmp_i1]);

		(tmp_interp, tmp_i1, tmp_i2)
	} else {
		let mut i = 0;

		while coarse_theta[i + 1] <= theta {
			i += 1;
		}

		let tmp_i1 = i;
		let tmp_i2 = i + 1;
		let tmp_interp = (theta - coarse_theta[tmp_i1]) / (coarse_theta[tmp_i2] - coarse_theta[tmp_i1]);

		(tmp_interp, tmp_i1, tmp_i2)
	};

	let x = (1. - interpolation) * coarse_contour[i1].x as f32 + interpolation * coarse_contour[i2].x as f32;
	let y = (1. - interpolation) * coarse_contour[i1].y as f32 + interpolation * coarse_contour[i2].y as f32;

	Point::new(x as i32, y as i32)
}
