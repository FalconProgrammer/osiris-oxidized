#![allow(dead_code)]
#![cfg_attr(backtrace, feature(backtrace))]

mod circle;
mod common_functions;
mod float_iterator;
mod opencv_normalize;
mod opencv_segment;
mod osi_error;

pub use circle::OsirisCircle;
pub use opencv_normalize::{
	normalize, normalize_from_contour, normalize_from_contour_with_segmented, normalize_with_segmented,
};
pub use opencv_segment::{segment_iris, OsirisSegmentedData, OsirisSerializableData, SerializablePoint};
pub use osi_error::{OsirisErrorKind, OsirisResult};
